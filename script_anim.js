const nameCssActive = 'active';
const menu = document.querySelector(".tabs");
const content = document.querySelector(".tabs-content");

menu.addEventListener("click", changeMenuItemActive);

function changeMenuItemActive(e) {
  if (e.target.nodeName !== "LI" || e.target.classList.contains("active")) {
    return;
  } else {
    clearStateActive(menu.children);
    setStateActive(e.target);
    changeContentItemActive(e);
  }
}

function changeContentItemActive(e) {
  const connectedContentItem = findConnectedContent(e.target);

  for (let i of content.children) {
    if (i.classList.contains(nameCssActive)) {
      i.style.opacity = 0;
      setTimeout(() => {
        i.style.display = 'none';
        i.classList.remove(nameCssActive);
      }, 600);
    }
  }

  if (connectedContentItem) {
    // setTimeout(() => {
    connectedContentItem.style.opacity = 0;
    connectedContentItem.style.display = 'inline-block';
    // }, 20);
    setTimeout(() => {
      connectedContentItem.style.opacity = 1;
      connectedContentItem.classList.add(nameCssActive);
    }, 300);
  }
  else {
    alert('Error: not finded connectedContentItem');
  }
}

function findConnectedContent(item) {
  for (let i of content.children) {
    if (i.dataset.article === item.dataset.article) {
      return i;
    }
  }
  return false;
}

function clearStateActive(item) {
  for (let i of item) {
    i.classList.remove(nameCssActive);
  }
}

function setStateActive(item) {
  item.classList.add(nameCssActive);
}